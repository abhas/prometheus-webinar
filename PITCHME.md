<!-- $theme: default -->

## ** Monitoring Websites with Prometheus **

### Abhas Abhinav
#### `DeepRoot Linux / Mostly Harmless / FSF - India
#### `https://abhas.io/` `abhas@deeproot.in`

---

## Agenda

Over the course of the next hour, we will learn to do the following:

* Learn about server and website **monitoring**
* Understand how to **setup** Prometheus
* **Configure** Prometheus to monitor servers
* **Visualise** server monitoring data
* **Monitor** websites uptime, performance and SSL certificate expiry
* Configure and manage **alerts**

---

## Introduction to Monitoring

There are several Free Software tools that enable us to monitor servers
and services. Some of the popular and *classical* monitoring systems
ones are [Nagios](https://nagios.org), [Zabbix](https://www.zabbix.com)
and [OpenNMS](http://opennms.org). 

Some recent innovations in monitoring are actually monitoring frameworks
like [Sensu](https://sensu.io/), [Reimann](http://riemann.io/) and
[Prometheus](https://prometheus.io/). 

---

## Pull v/s Push Monitoring

Two methods of monitoring servers and services:

* Push-based Monitoring
  Systems that you want to monitor run an *agent* which sends data and metrics to a central server. *Eg. Zabbix*

* Pull-based Monitoring
  The central servers *pulls* data and metrics from the monitored server. *Eg. Prometheus*

---

## Prometheus - Pull Monitoring

Prometheus is a **pull-based** monitoring system. It expects applications, servers and services to expose a HTTP-based endpoint. It then *scrapes* this endpoint and stores the metrics in a time-series database. An application can either have native support for Prometheus *(Gitlab, Kubernetes, Docker)* or can be assisted by an *exporter*.

---

## What is a metric?

A metric is a property of a hardware or software. Metrics are
data-points that are observed and recorded over time. These are values, timestamps and other such properties.

```
node_hwmon_fan_rpm 3809
node_load15 0.98
```

**Time-series**: A time-series is a metric that is measured over time and is stored as a pair of a timestamp and a metric

---

## Architecture

---

![](assets/img/prom-arch.png)

---

## Prometheus Capabilities

* Monitor servers, services, hardware and IoT!
* Store metrics
* Provides a querying and graphing interface
* Export metrics to visualisation tools
* Send alerts

---

## Prometheus components

* The `prometheus` daemon
* `node_exporter` to export node / hardware metrics
* `alertmanager` for sending alerts
* `pushgateway` to receive (and publish) push metrics
* `blackbox_exporter` to monitor websites and protocols as a *blackbox*

---

![](assets/img/prom-tools.png)

---

## Quick Start

  1. Visit: [`github.com/prometheus`](https://github.com/prometheus)
  2. Download [`prometheus`](https://github.com/prometheus/prometheus) and [`node_exporter`](https://github.com/prometheus/node_exporter)
  3. Run `node_exporter`
  4. Configure and run `prometheus`
  5. Visit: [`localhost:9090`](http://localhost:9090)

---

## Download

### prometheus

```bash
wget prometheus-2.5.0.linux-amd64.tar.gz
```

### node_exporter

```bash
wget node_exporter-0.17.0.linux-amd64.tar.gz
```

---

## Extract

```bash
tar xvfz prometheus-2.5.0.linux-amd64.tar.gz
tar xvfz node_exporter-0.17.0.linux-amd64.tar.gz
```

---

## Run `node_exporter`

```
cd node_exporter-0.17.0.linux-amd64/
sudo ./node_exporter
```

![](assets/img/node-exporter-output-split.png)

```
INFO[0000] Listening on :9100
```

---

## View node metrics

Now you can visit [`localhost:9100/metrics`:](http://localhost:9100/metrics)

### Load average

```
# HELP node_load1 1m load average.
# TYPE node_load1 gauge
node_load1 0.94
# HELP node_load15 15m load average.
# TYPE node_load15 gauge
node_load15 0.63
# HELP node_load5 5m load average.
# TYPE node_load5 gauge
node_load5 0.75
```

---

### Network interfaces

```
# TYPE node_network_address_assign_type gauge
node_network_address_assign_type{interface="br-411a250a1f94"} 3
node_network_address_assign_type{interface="br-65ab7224988f"} 3
node_network_address_assign_type{interface="docker0"} 3
node_network_address_assign_type{interface="enp0s25"} 0
node_network_address_assign_type{interface="lo"} 0
node_network_address_assign_type{interface="tun0"} 0
node_network_address_assign_type{interface="tun1"} 0
node_network_address_assign_type{interface="tun2"} 0
node_network_address_assign_type{interface="tun3"} 0
node_network_address_assign_type{interface="tun4"} 0
node_network_address_assign_type{interface="tun5"} 0
node_network_address_assign_type{interface="tun6"} 0
node_network_address_assign_type{interface="tun7"} 0
node_network_address_assign_type{interface="veth670bb37"} 1
node_network_address_assign_type{interface="vethff9b0f7"} 1
node_network_address_assign_type{interface="wlp2s0"} 3
```

---

### Filesystem usage

```
# TYPE node_filesystem_avail_bytes gauge
node_filesystem_avail_bytes{device="/dev/mapper/vg-root",fstype="ext4",mountpoint="/"} 2.3050981376e+10
node_filesystem_avail_bytes{device="/dev/sda1",fstype="ext2",mountpoint="/boot"} 1.06015744e+08
node_filesystem_avail_bytes{device="/dev/sdb1",fstype="ext4",mountpoint="/media/mSATA"} 9.291460608e+09
```

---

## Run Prometheus

Lets start with the default sample configuration file:

```
$ ./prometheus --config.file="prometheus.yml"
level=info [...] msg="Completed loading of configuration file" filename=prometheus.yml
level=info [...] msg="Server is ready to receive web requests."

```

---

## Configure Prometheus

```
global:
  scrape_interval:     15s
  evaluation_interval: 15s

scrape_configs:
  - job_name: 'prometheus'
    static_configs:
    - targets: ['localhost:9090']
```

---

## Access Prometheus UI

[`localhost:9090/targets`](http://localhost:9090/targets)

![](assets/img/prom-ui.png)

---

### Add Targets

```
scrape_configs:
  - job_name: 'prometheus'
    static_configs:
    - targets: ['localhost:9090']
  - job_name: 'node'
    static_configs:
    - targets: ['localhost:9100']
```
---

![](assets/img/node-target.png)

---

## Queries

Lets query the CPU temperature: `node_hwmon_temp_celsius`

![](assets/img/query1.png)

---

![](assets/img/query2.png)

---

## Graphs

---

![](assets/img/query3.png)

---

### Summary

The `node_exporter` provides a very simple way to monitor all sorts of
details about a physical server or a virtual machine. Just like we added
the IP and port number of the ndoe in the `scrape_configs`, we can add
other scrape nodes and exporters as well.

Prometheus will then store the data and enable us to query and graph it.

---

## Introducing the blackbox exporter

The `blackbox_exporter` is a native Prometheus feature to monitor
services and protocols without any configuration on the part of what we
are monitoring. Unlike the *exporter* method, the `blackbox_exporter`
talks to the service as a client and uses information available to
generate metrics that can be monitored and stored.

---

### Blackbox Exporter Modules

  * **`http`**: lets us monitor websites over http(s)
  * **`dns`**: lets us monitor DNS records
  * **`icmp`**: uses `icmp` to monitor remote hosts
  * **`tcp`**: enables monitoring of *any* TCP-based protocol

---

### Architecture

The blackbox exporter talks to a host / service using the corresponding
module and applies the checks described in the config file.

It then makes the *details* available on its `/probe` endpoint
so that prometheus can *scrape* these metrics. Once Prometheus is able
to scrape them, then they get stored in the database and become
available for querying, graphing and alerting.

---

#### Blackbox Exporter Configuration

`blackbox.yml:`

```
modules:
  http_check:
    prober: http
    timeout: 5s
    http:
      valid_http_versions: ["HTTP/1.1", "HTTP/2"]
      valid_status_codes: []
      method: GET
      no_follow_redirects: false
      tls_config:
        insecure_skip_verify: false
      preferred_ip_protocol: "ip4"
      fail_if_matches_regexp:
        - "Could not connect to database"
```

---

#### Download and run 

```
wget blackbox_exporter-0.13.0.linux-amd64.tar.gz
tar xvfz blackbox_exporter-0.13.0.linux-amd64.tar.gz
cd blackbox_exporter-0.13.0.linux-amd64
./blackbox_exporter --config.file="blackbox.yml"
```

![](assets/img/run-blackbox.png)

---

## Test run

---

`http://localhost:9115/probe?target=https://deeproot.in&module=http_check`

```
probe_dns_lookup_time_seconds 0.066783426
probe_duration_seconds 0.329059907
probe_failed_due_to_regex 0
probe_http_content_length -1
probe_http_duration_seconds{phase="connect"} 0.023044191
probe_http_duration_seconds{phase="processing"} 0.048574649
probe_http_duration_seconds{phase="resolve"} 0.066783426
probe_http_duration_seconds{phase="tls"} 0.207868047
probe_http_duration_seconds{phase="transfer"} 0.001136159
probe_http_redirects 0
probe_http_ssl 1
probe_http_status_code 200
probe_http_version 1.1
probe_ip_protocol 4
probe_ssl_earliest_cert_expiry 1.547949946e+09
probe_success 1
```
---

`http://localhost:9115/probe?target=https://www.gnu.org&module=http_check`

```
probe_dns_lookup_time_seconds 0.001760497
probe_duration_seconds 1.239652591
probe_failed_due_to_regex 0
probe_http_content_length -1
probe_http_duration_seconds{phase="connect"} 0.304137434
probe_http_duration_seconds{phase="processing"} 0.314871634
probe_http_duration_seconds{phase="resolve"} 0.001760497
probe_http_duration_seconds{phase="tls"} 0.92064817
probe_http_duration_seconds{phase="transfer"} 0.002067702
probe_http_redirects 0
probe_http_ssl 1
probe_http_status_code 200
probe_http_version 1.1
probe_ip_protocol 4
probe_ssl_earliest_cert_expiry 1.54946117e+09
probe_success 1
```

---

## Connecting Prometheus to the Blackbox Exporter

---

Add to `prometheus.yml` and restart:

```
  - job_name: 'blackbox'
    metrics_path: /probe
    params:
      module: [http_check]
    static_configs:
      - targets:
        - http://deeproot.in
        - http://www.gnu.org
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: localhost:9115
```

---

![](assets/img/blackbox-target.png)

---

## Alertmanager

Alert rules defined in Prometheus are passed on to Alertmanager which then send
out the actual alerts via email, SMS, chat or any other custom method.

---

Setup and run alertmanager:

```
wget alertmanager-0.15.3.linux-amd64.tar.gz
tar xvfz alertmanager-0.15.3.linux-amd64.tar.gz
cd alertmanager-0.15.3.linux-amd64
```

---

Alertmanager configuration:

```
global:
	smtp_smarthost: 'localhost:1025'
	smtp_from: 'alerts@abhas.io'
	smtp_require_tls: false

templates:
	- '/etc/alertmanager/template/*.tmpl'

route:
	receiver:email

receivers:
	- name: 'email'
		email_configs:
			- to: 'abhas@deeproot.in'
```

---

Run alertmanager:

```
./alertmanager --config.file="alertmanager.yml"
```

```
caller=main.go:174 msg="Starting Alertmanager" version="(version=0.15.3, branch=HEAD, revision=d4a7697cc90f8bce62efe7c44b63b542578ec0a1)"
caller=main.go:175 build_context="(go=go1.11.2, user=root@4ecc17c53d26, date=20181109-15:40:48)"
caller=cluster.go:155 component=cluster msg="setting advertise address explicitly" addr=192.168.54.158 port=9094
caller=main.go:322 msg="Loading configuration file" file=alert.yml
caller=cluster.go:570 component=cluster msg="Waiting for gossip to settle..." interval=2s
caller=main.go:398 msg=Listening address=:9093
```

---

![](assets/img/alert1.png)

---

Add to `prometheus.yml` and restart:

```
alerting:
  alertmanagers:
    - static_configs:
      - targets:
        - alertmanager:9093
```

---

Build rules to send alerts - `ssl_expiry.yml` - This sends an alert if the
certificate expiry date is within 30 days.

```
groups: 
  - name: ssl_expiry.rules 
    rules: 
    - alert: SSL_Cert_Expiry
        expr: probe_ssl_earliest_cert_expiry{job="blackbox"} - time() < 86400 * 10 
        for: 10m
```

---

But no condition satifies the rule...

![](assets/img/alert3.png)

---

Let us change the expiry window to 1000 days - Now you see the alert!

![](assets/img/alert2.png)

---

Alertmanager also sees this alert:

![](assets/img/alert5.png)

---

This is what the query which generated the alert looks like:

![](assets/img/alert-query.png)

---

And it sends out an email too:

![](assets/img/mail2.png)

---

Similarly, we can monitor for high CPU usage, disk usage, network usage and
just about anything that Prometheus can scrape and monitor.

---

The blackbox exporter can test for the presence of strings in the output of a
web page as well and raise alerts. This example in the Prometheus source code
has more such examples:

[`https://github.com/prometheus/blackbox_exporter/blob/master/example.yml`](https://github.com/prometheus/blackbox_exporter/blob/master/example.yml)

---

### Grafana Integration

Grafana is a web-based monitoring dashboard that allows you query, visualize
and alert on metrics. It can talk to Prometheus among other time-series datbases such as 
InfluxDB, OpenTSD and so on.

Deploy Grafana locally is using Docker:

```
docker run -d -p 3000:3000 --net=host grafana/grafana
```

---

### Steps for Grafana Integration

1. Login to Grafana as an admin
2. Configure a data source (prometheus in our case)
3. Add or import dashboards to view metrics

---
### Grafana UI

Visit: `http://localhost:3000`

![](assets/img/grafana1.png)

---
Grafana - Add Data Source

![](assets/img/grafana2.png)

---

Prometheus Data Source

![](assets/img/grafana3.png)

---

Node Exporter Dashboard - Dashboard ID: `1860` 

![](assets/img/grafana7.png)

---

Blackbox Exporter Dashboard - Dashboard ID: `7587`

![](assets/img/grafana6.png)

![](assets/img/grafana5.png)

---

### Book

https://www.prometheusbook.com/

![](assets/img/prombook.png)

---

### Contact Details

* Email: [`abhas@deeproot.in`](mailto:abhas@deeproot.in)
* Blog: [`abhas.io`](https://abhas.io)

-----

#### Presentation source code:

[`gitlab.com/abhas/prometheus-webinar/`](https://gitlab.com/abhas/prometheus-webinar/)

