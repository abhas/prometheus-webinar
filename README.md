## Welcome to a small presentation on Monitoring with Prometheus

* Learn about server and website **monitoring**
* Understand how to **setup** Prometheus
* **Configure** Prometheus to monitor servers
* **Visualise** server monitoring data
* **Monitor** website performance and SSL certificate expiry
* Configure and manage **alerts**

## View the slides using a browser

<https://gitpitch.com/abhas/prometheus-webinar?grs=gitlab>

## View the slides in Gitlab

<https://gitlab.com/abhas/prometheus-webinar/blob/master/PITCHME.md>

